import AuthForm from '../components/Auth/AuthForm';

const AuthPage = () => {
    return (
      <div className='container'>
        <AuthForm />

      </div>
    )
};

export default AuthPage;
