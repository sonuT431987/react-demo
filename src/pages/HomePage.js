
import BannerGroup from '../components/Banner/Banner';
import Post from '../components/Post/Posts';
import RecentPost from '../components/Post/RecentPost';

const HomePage = () => {
	
  return (
    <section className='content'>
		<BannerGroup />
		<RecentPost />
		<Post />
    </section>
  );
};

export default HomePage;