import ContactForm from "../components/ContactUs/ContactForm";

const ContactPage = () => {
  return <ContactForm />;
};

export default ContactPage;
