import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import i18n from "i18next";
import { useTranslation } from "react-i18next";
import "../Post/Posts.css";
import RecentSliderSkelton from "../Skelton/RecentSlider.js";

const url = 'https://jsonplaceholder.typicode.com/photos?_page=2&_limit=100';

const RecentPost = () => {
        const [posts, setPosts] = useState([]);
		const [hasError, setHasError] = useState(false);
		const [isLoading, setIsLoading] = useState(true);

		useEffect(() => {
			const fetchData = async () => {
			  setIsLoading(true);
			  setHasError(false);
			  try {
				const res = await fetch(url,
				);
				const posts = await res.json();
				setPosts(posts);
			  } catch (error) {
				setHasError(true);
			  }
			  setIsLoading(false);
			};
			fetchData();
		  }, [setPosts]);


    const sliderSettings = {
        slidesToShow: 8,
        infinite: false,
        autoplay: true,
        lazyLoad: true,
        arrows: true,
		responsive: [
			{
			breakpoint: 1024,
			settings: {
				slidesToShow: 5,
				slidesToScroll: 3,
			}
			},
			{
			breakpoint: 600,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 2,
				initialSlide: 2
			}
			},
			{
			breakpoint: 480,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
			}
		]
    };

	const { t } = useTranslation();
	useEffect(() => {
	  let currentLang = localStorage.getItem('lang');
	  // console.log(currentLang);
	  i18n.changeLanguage(currentLang);
	},[]);
  return (
	  <div className="container">
		<div className="slickwrapper">
		<h2>{t('latestblog')}</h2>      
		{hasError && <p>Something went wrong.</p>}
			{
				isLoading ?
				<div className="postwrap">
					<RecentSliderSkelton />
					<RecentSliderSkelton />
					<RecentSliderSkelton />
					<RecentSliderSkelton />
					<RecentSliderSkelton />
					<RecentSliderSkelton />
					<RecentSliderSkelton />
					<RecentSliderSkelton />
					</div>
				:
				<Slider {...sliderSettings}>
				{
				posts.map((item, index) => (
					<div key={index}>
					<Link to={"/BlogDetails/" + item.id}><img
						className="img"
						alt={item.title}
						src={item.thumbnailUrl}
					/></Link>
					<div className="blog-title">
						<Link to={"/BlogDetails/" + item.id}>{item.title}</Link>
					</div>
					</div>
				))}
				</Slider>
			}
		
		</div>
	</div>
  );
};

export default RecentPost;
