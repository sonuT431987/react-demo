import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Pagination from "../Pagination/Pagination";
import BlogSkelton from "../Skelton/BlogThumb";
import i18n from "i18next";
import { useTranslation } from "react-i18next";

import "../Post/Posts.css";

const url = "https://jsonplaceholder.typicode.com/photos?_page=3&_limit=100";

let PageSize = 20;
const Post = (props) => {
	const [posts, setPosts] = useState([]);
	const [currentPage, setCurrentPage] = useState(1);
	const [isLoading, setIsLoading] = useState(true);
	const [hasError, setHasError] = useState(false);
	// const [data, setData] = useState([]);
	// const [state, setState] = useState();

	const [sortType, setSortType] = useState('title');
	const firstPageIndex = (currentPage - 1) * PageSize;
	const lastPageIndex = firstPageIndex + PageSize;


  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setHasError(false);
      try {
        const res = await fetch(url,
        );
        const posts = await res.json();
        setPosts(posts);
      } catch (error) {
        setHasError(true);
      }
      setIsLoading(false);
    };
    fetchData();
  }, [setPosts]);

  const { t } = useTranslation();
  useEffect(() => {
    let currentLang = localStorage.getItem('lang');
    // console.log(currentLang);
    i18n.changeLanguage(currentLang);
  },[]);

  useEffect(() => {
	const sortArray = type => {
		const types = {
		  id: 'id',
		  title: 'title',
		};
		const sortProperty = types[type];
		const sorted = posts.sort((a, b) => b[sortProperty] - a[sortProperty]);
		// console.log(sorted);
		setSortType(sorted);
	  };
	  sortArray(sortType);
}, [sortType, posts]);

// console.log(setSortType);


  return (
    <div className="container">
      <h2 className="headwrap">{t('blog')} <span> Sort By :  
		<select className="choosen" onChange={(e) => setSortType(e.target.value)}> 
			<option value="id">ID</option>
			<option value="title">Title</option>
		</select>
        </span></h2>
      <div className="postwrap">
	  {hasError && <p>Something went wrong.</p>}
	  {
			  isLoading ?
			  <>
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				<BlogSkelton />
				</>
			  :

			  posts.slice(firstPageIndex, lastPageIndex).map((post, index) => (
				<div className="card" key={post.id}>
				  <div className="card-img-holder">
					<Link to={"/BlogDetails/" + post.id}>
					  <img src={post.thumbnailUrl} alt="Post" />
					</Link>
				  </div>
				  <div className="blog-title">
					<Link to={"/BlogDetails/" + post.id}>{post.title}</Link>
				  </div>
				</div>
			  ))
	  }
      </div>
      <Pagination
        className="pagination-bar"
        currentPage={currentPage}
        totalCount={posts.length}
        pageSize={PageSize}
        onPageChange={(page) => setCurrentPage(page)}
      />
    </div>
  );

  // const { title, thumbnailUrl } = props.data;
  // return (
  //   <div className="card">
  // 	<div className="card-img-holder">
  // 		<img src={thumbnailUrl} alt="Post" />
  // 	</div>
  // 	<div className="blog-title">{title}</div>
  //   </div>
  // );
};

export default Post;
