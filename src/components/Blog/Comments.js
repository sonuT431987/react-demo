import React, { useState, useEffect } from "react";
import Pagination from "../Pagination/Pagination";
import CommentsSkelton from "../Skelton/CommentsSkelton";
import './Comments.css';

const url = "https://jsonplaceholder.typicode.com/comments?_page=3&_limit=10";

const Comments = () => {
    let PageSize = 4;
    const [posts, setPosts] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [hasError, setHasError] = useState(false);

    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;

    useEffect(() => {
    const fetchData = async () => {
        setIsLoading(true);
        setHasError(false);
        try {
        const res = await fetch(url,
        );
        const posts = await res.json();
        setPosts(posts);
        } catch (error) {
        setHasError(true);
        }
        setIsLoading(false);
    };
    fetchData();
    }, [setPosts]);

    return (
        <div className="commentwrap">
            <h2>Comments</h2>
            {hasError && <p>Something went wrong.</p>}
	        {
			  isLoading ?
			  <>
				<CommentsSkelton />
				<CommentsSkelton />
				<CommentsSkelton />
				<CommentsSkelton />
				<CommentsSkelton />
				</>
			  :

              posts.slice(firstPageIndex, lastPageIndex).map((post, index) => (
				<div className="commentspost" key={post.id}>
                    <div className="name">{post.name}</div>
                    <div className="email">{post.email}</div>
                    <div className="body">{post.body}</div>                        
				</div>
			  ))
            }
            <Pagination
                className="pagination-bar"
                currentPage={currentPage}
                totalCount={posts.length}
                pageSize={PageSize}
                onPageChange={(page) => setCurrentPage(page)}
            />
        </div>
    )
}

export default Comments;