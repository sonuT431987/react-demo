import React from "react";
import { useEffect } from "react";
import i18n from "i18next";
import { useTranslation } from "react-i18next";
import './Footer.css';


const Footer = () => {
    const { t } = useTranslation();
    useEffect(() => {
		let currentLang = localStorage.getItem('lang');
		// console.log(currentLang);
		i18n.changeLanguage(currentLang);
    },[]);
    return (
        <footer className="footer">
            {t('copyright')}
        </footer>
    )
}

export default Footer;