import React from "react";
import { useEffect } from "react";
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import commonEn from '../translation/en/common.json';
import commonTh from '../translation/th/common.json';


i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: commonEn
      },
      th: {
        translation: commonTh
      },

    },
    lng: "en", // if you're using a language detector, do not define the lng option
    fallbackLng: "en",

    interpolation: {
      escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
    }
  });

const changeLang = (l)=>{
	return ()=>{
	i18n.changeLanguage(l);

	localStorage.setItem('lang',l);

	}
}

const Lang = ()=> {

	const { t } = useTranslation();
    useEffect(() => {
		let currentLang = localStorage.getItem('lang');
		i18n.changeLanguage(currentLang);
   
    },[]);
    return (
		<>
			<span className="langitems" onClick={changeLang('en')}>{ t('en') }</span>
      		<span>/</span>
			<span className="langitems" onClick={changeLang('th')}>{ t('th') }</span>
		</>
	)
}

export default Lang;