import React from "react";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import logo from "./../../logo.svg";
import classes from "./Header.module.css";
import i18n from "i18next";
import { useTranslation } from "react-i18next";
import Lang from "./Lang";

const Header = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const { t } = useTranslation();
  useEffect(() => {
    const loginInfo = localStorage.getItem("isLoggedIn");
    // console.log(loginInfo);
    if (loginInfo === "1") {
      setIsLoggedIn(true);
    }
    let currentLang = localStorage.getItem("lang");
    i18n.changeLanguage(currentLang);
  }, []);


  const logoutHandler = () => {
    localStorage.removeItem("isLoggedIn");
    setIsLoggedIn(false);
    localStorage.removeItem("isUser");
  };



  return (
    <header className={classes.headers}>
      <div className="container">
        <div className={classes.header}>
          <NavLink to="/">
            <div className={classes.logo}>
              <img src={logo} alt="Logo" />
            </div>
          </NavLink>
          <nav>
            <ul>
              <li>
                <NavLink className={classes.active} to="/">
                  {t("home")}
                </NavLink>
              </li>
              <li>
                <NavLink className={classes.active} to="/blog">
                  {t("all_Blog")}
                </NavLink>
              </li>
              <li>
                <NavLink className={classes.active} to="/contactus">
                  {t("contact_us")}
                </NavLink>
              </li>
            </ul>
          </nav>
          <div className={classes.authWrap}>
            <ul>
              {!isLoggedIn && (
                <li>
                  <NavLink to="/login" className={classes.logout}>
                    {t("login")}
                  </NavLink>
                </li>
              )}
              {isLoggedIn && (
                <li>
                  <NavLink to="/dashboard" className={classes.logout}>
                    User Name
                  </NavLink>
                  <span className={classes.logout} onClick={logoutHandler}>
                    {t("logout")}
                  </span>
                </li>
              )}
              <li>
                <Lang />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
