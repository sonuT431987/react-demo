import React, {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import './AuthForm.css'

const AuthForm = (props) => {
	const [errorMessages, setErrorMessages] = useState({});
	const [isSubmitted, setIsSubmitted] = useState(false);

	const [isLoggedIn, setIsLoggedIn] = useState(false);
    
	let history = useHistory();
	// User Login info
	const database = [
		{
		username: "user1",
		password: "pass1"
		},
		{
		username: "user2",
		password: "pass2"
		}
	];

	const errors = {
		uname: "invalid username",
		pass: "invalid password"
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		var { uname, pass } = document.forms[0];
		const userData = database.find((user) => user.username === uname.value);
		if (userData) {
			if (userData.password !== pass.value) {
				// Invalid password
				setErrorMessages({ name: "pass", message: errors.pass });
			} else {
				setIsSubmitted(true);
				history.push('/');
				localStorage.setItem("isLoggedIn", "1");
				setIsLoggedIn(true);
				window.location.reload(false);
		}
		} else {
		// Username not found
		setErrorMessages({ name: "uname", message: errors.uname });
		}
        		
	};
	console.log(isSubmitted);
	console.log(isLoggedIn);

    useEffect(() => {
        const loginInfo = localStorage.getItem('isLoggedIn');
        if (loginInfo === '1') {
            setIsLoggedIn(true);
        }
    }, []);

    const loginHandler = () => {
      localStorage.setItem("isLoggedIn", "1");
      setIsLoggedIn(true);
    };

	// Generate JSX code for error message
	const renderErrorMessage = (name) =>
		name === errorMessages.name && (
		<div className="error">{errorMessages.message}</div>
		);


	return (
		<div className="login-form">
        	<div className="title">Login</div>
			<form onSubmit={handleSubmit}>
				<div className="input-container">
					<label>Username </label>
					<input type="text" name="uname" required />
					{renderErrorMessage("uname")}
				</div>
				<div className="input-container">
					<label>Password</label>
					<input type="password" name="pass" required />
					{renderErrorMessage("pass")}
				</div>
				<div className="button-container">
					<input type="submit" onClick={loginHandler.bind(this)}  />
				</div>
			</form>
      	</div>
	)
}

export default AuthForm;
