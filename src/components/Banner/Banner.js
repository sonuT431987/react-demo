import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import classes from "./BannerGroup.module.css";

const BannerGroup = () => {

	const BannerImg = [
		{
			id: "1",
			imageSrc:
			"https://images.unsplash.com/photo-1599619351208-3e6c839d6828?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=872&q=80",
			title: "Banner One",
		},
		{
			id: "2",
			imageSrc:
			"https://images.unsplash.com/photo-1461092746677-7b4afb1178f6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80",
			title: "Banner Two",
		},
	];

	const sliderSettings = {
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: false,
	};
  return (
    <div className={classes.bannergroup}>
			 {
				<Slider {...sliderSettings}>
					{BannerImg.map((banner, index) => (
					<div key={index}>
						<img
						className={classes.imgbanner}
						alt={banner.title}
						src={banner.imageSrc}
						/>
					</div>
					))}
				</Slider>
			 }

    </div>
  );
};

export default BannerGroup;
