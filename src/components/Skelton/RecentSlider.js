import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";


const RecentSliderSkelton = () => {
  return (
      <div className="cards">
        <div className="card-img-holder">
          <Skeleton height={150} width={150} />
        </div>
        <div className="blog-title">
          <Skeleton count={1} />
        </div>
      </div>
  );
};

export default RecentSliderSkelton;