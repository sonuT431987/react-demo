import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";


const CommentsSkelton = () => {
  return (
    <div className="commentspost">
        <Skeleton height={50} />
        <Skeleton height={50} />
        <Skeleton height={50} />

    </div>
  );
};

export default CommentsSkelton;