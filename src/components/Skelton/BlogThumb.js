import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const BlogSkelton = () => {
  return (
    <div className="card">
      <div className="card-img-holder">
        <Skeleton height={250} />
      </div>
      <div className="blog-title">
        <Skeleton count={1} />
      </div>
    </div>
  );
};

export default BlogSkelton;