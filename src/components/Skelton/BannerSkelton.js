import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const BannerSkelton = () => {
  return (
    <div className="banner">
      <div className="card-img-holder">
        <Skeleton height={500} />
      </div>
    </div>
  );
};

export default BannerSkelton;