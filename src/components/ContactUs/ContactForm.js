import React, {useState} from "react";
import './ContactUs.css';

const ContactForm = ()=> {
    const [userRegistration, setUserRegistration] = useState({
        username: "",
        email: "",
        phone: "",
        password: ""
    });
    const [records, setRecords] = useState([]);
    const handleInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setUserRegistration({...userRegistration, [name]: value});

    }
    const handleSubmit = (e) => {
        e.preventDefault();
        
        const newRecords = {...userRegistration, id: new Date().getTime().toString()};
       setRecords([...records, newRecords]);
        console.log(records);
        setUserRegistration({username: "", email: "", phone: "", password: ""});
    };
    return (
        <div className="container">
            <h1 className="title">ContactForm</h1>
            <div className="">
                {
                    records.map((elm) => {
                        return (
                            <div key={elm.id} className="records">
                                <p>{elm.username}</p>
                                <p>{elm.email}</p>
                                <p>{elm.phone}</p>
                                <p>{elm.password}</p>
                            </div>
                        )
                    }) 
                }
            </div>
            <form className="contactform" action="" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="username">Full Name</label>
                    <input type="text" value={userRegistration.username} onChange={handleInput} autoComplete="off" name="username" id="username" />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="text" value={userRegistration.email} onChange={handleInput} autoComplete="off" name="email" id="email" />
                </div>
                <div className="form-group">
                    <label htmlFor="phone">Phone</label>
                    <input type="text" value={userRegistration.phone} onChange={handleInput} autoComplete="off" name="phone" id="phone" />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Password</label>
                    <input type="text" value={userRegistration.password} onChange={handleInput} autoComplete="off" name="password" id="password" />
                </div>
                <button type="submit" className="submitButton">Send</button>
            </form>
        </div>
    )
}

export default ContactForm;