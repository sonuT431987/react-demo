import React, {Suspense} from 'react';
import { Switch, Route } from 'react-router-dom';
import LoadingSpinner from './components/UI/LoadingSpinner';
import Header from './components/Layout/Header';
import Footer from './components/Layout/Footer';
import Dashboard from './components/Auth/Dashboard/Dashboard';
const HomePage = React.lazy(() => import('./pages/HomePage'));
const AuthPage = React.lazy(() => import('./pages/AuthPage'));
const ContactForm = React.lazy(() => import('./components/ContactUs/ContactForm'));
const BlogPage = React.lazy(() => import('./pages/BlogPage'));
const BlogDetails = React.lazy(() => import('./components/Blog/BlogDetails'));

function App() {
  return (
	  <div>
		<Header />
		<Suspense fallback={
		<LoadingSpinner />
		}>
			<Switch>
				<Route path='/' exact>
					<HomePage />
				</Route>
				<Route path='/login'>
					<AuthPage />
				</Route>
				<Route path='/dashboard'>
					<Dashboard />
				</Route>
				<Route path="/blogDetails/:blogId">
					<BlogDetails />
				</Route>
				<Route path='/blog'>
					<BlogPage />
				</Route>
				<Route path='/contactus'>
					<ContactForm />
				</Route>
			</Switch>
	  </Suspense>
	  <Footer />
	  </div>

  );
}

export default App;
